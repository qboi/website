---
layout: page
title: index
permalink: /index/
---

# Quasi Biennial Oscillation initiative (QBOi)

QBOi is an activity of the Stratosphere-troposphere Processes And their Role in Climate (SPARC) core project of the World Climate Research Programme (WCRP).

[SPARC QBOi website](https://www.sparc-climate.org/activities/quasi-biennial-oscillation/)

## Experiments

Experiment protocols here

## Phase-1

[QJRMS Special Collection with QBOi Phase-1 analyses](https://rmets.onlinelibrary.wiley.com/doi/toc/10.1002/(ISSN)1477-870X.qbo-modelling-intercomparison)

